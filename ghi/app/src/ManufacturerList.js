import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);
    const fetchManufacturers = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    };

    useEffect(() => {
        fetchManufacturers();
    }, []);

    return (
        <>
            {manufacturers.length ? (
                <div>
                    <br></br>
                    <p className="h1">Manufacturer List</p>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {manufacturers.map((manufacturer) => {
                                return (
                                    <tr key={manufacturer.id}>
                                        <td>{manufacturer.name}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            ) : (
                <>
                    <h1>No Manufacturers</h1>
                    <p>There are no manufacturers in the database</p>
                    <NavLink
                        to="/manufacturers/create"
                        className="btn btn-success"
                    >
                        Add a Manufacturer
                    </NavLink>
                </>
            )}
        </>
    );
}

export default ManufacturerList;
