import React, { useState } from 'react';

function SalespersonForm() {
    const [firstName, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value);
    }

    const [lastName, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
    }

    const [employeeID, setEmployeeID] = useState('');
    const handleEmployeeIDChange = (event) => {
        setEmployeeID(event.target.value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID;
        const url = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const salespersonResponse = await fetch(url, fetchConfig);
        if (salespersonResponse.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeID('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Sales Person</h1>
                    <form onSubmit={handleSubmit} id="create-employee-form">
                        <div className="row g-2 mb-4">
                            <div className="col-md">
                                <div className="form-floating">
                                    <input onChange={handleFirstNameChange} type="text" className="form-control" id="firstName" value={firstName}/>
                                    <label htmlFor="firstName">First Name</label>
                                </div>
                            </div>
                            <div className="col-md">
                                <div className="form-floating">
                                    <input onChange={handleLastNameChange} type="text" className="form-control" id="lastName" value={lastName}/>
                                    <label htmlFor="lastName">Last Name</label>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 mb-4">
                            <div className="col-md mb-4">
                                <div className="form-floating">
                                    <input onChange={handleEmployeeIDChange} type="text" className="form-control" id="employeeID" value={employeeID}/>
                                    <label htmlFor="employeeID">Employee ID</label>
                                </div>
                            </div>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SalespersonForm;