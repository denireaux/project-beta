import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()


from sales_rest.models import AutomobileVO


def get_auto_data():
    response = requests.get(
        "http://partner-final-project-inventory-api-1:8000/api/automobiles/"
    )
    content = json.loads(response.content)
    for auto in content["autos"]:
        AutomobileVO.objects.update_or_create(
            defaults={
                "vin": auto["vin"],
                "sold": auto["sold"],
            },
        )


def poll():
    while True:
        print("Polling for data")
        try:
            get_auto_data()
        except Exception as e:
            print(e, file=sys.stderr)

        time.sleep(60)


if __name__ == "__main__":
    poll()
